import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { Client } from '@stomp/stompjs'; /* Nos suscribimos al evento chat */
import * as SockJS from 'sockjs-client'; /*  */
import { Message } from './models/message';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css'],
})
export class ChatComponent implements OnInit {
  private client: Client;
  connected: boolean = false;
  message: Message = new Message();
  messages: Message[] = [];
  /**
   * Cada mensaje que va a ser escuchado por el suscriptor
   * en el evento /chat/ va a ser almacenado en el array
   * de mensajes, que es lo que luego va a mostrarse en pantalla.
   */

  constructor() {}

  ngOnInit() {
    this.client = new Client();
    this.client.webSocketFactory = () => {
      /* npm install @stomp/stompjs@5.2.0 */
      return new SockJS('http://localhost:8080/chat-websocket');
      /*
       *  Ruta URL de la conexión del broker.
       *  Misma de Spring.
       **/
    };
    this.client.onConnect = (frame) => {
      console.log('Conectados: ' + this.client.connected + ' : ' + frame);
      this.connected = true;
      this.client.subscribe('/chat/message', (e) => {
        let message: Message = JSON.parse(e.body) as Message;
        message.date = new Date();
        this.messages.push(message);
        console.log('chat.component.ngOnInit.message');
        console.log(message);
      });
      /**
       * Nos tenemos que SUSCRIBIR y ESCUCHAR al evento
       * que creamos en el controlador del lado backend (/chat/message/)
       * ya que el broker (servidor) de WebSocket va a mostrar los mensajes
       * recuperados del lado del backend.
       * A su vez, vamos a enviar los mensajes que recuperamos en el front
       * al endpoint que seteamos en el backend @MessageMapping --> ("/message").
       */
    };

    this.client.onDisconnect = (frame) => {
      console.log('Desconectados: ' + !this.client.connected + ' : ' + frame);
    };
  }

  connect(): void {
    this.client.activate();
  }

  disconnect(): void {
    this.client.deactivate();
  }

  sendMessage(): void {
    this.client.publish({
      destination: '/app/message', 
      /** 
       * Agregamos el destino donde vamos a enviar cada mensaje (message).
       * Debemos especificar correctamente el PREFIJO (/api)
       * y el endpoint definido en el @MappingMessage --> ("/message")
       */
      body: JSON.stringify(this.message),
      /**
       * Agregamos el mensaje a enviar al backend.
       */
    });
  }
}
