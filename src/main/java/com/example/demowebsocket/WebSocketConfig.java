package com.example.demowebsocket;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

@Configuration
@EnableWebSocketMessageBroker /* Habilita el servidor o el broker de WebSocket en Spring. */
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/chat-websocket") /* Ruta para conectarnos desde Angular a este servidor broker. */
                .setAllowedOrigins("http://localhost:4200") /* Seteamos el CORS para Angular. */
                .withSockJS(); /* Por debajo STOMP utiliza SockJS, para conectarnos al broker correctamente
                                  utilizando el protocolo HTTP.
                                */
    }

    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        registry.enableSimpleBroker("/chat/"); /* Se setea el prefijo para los nombres del evento
                                                                  cuando el servidor emite un mensaje y quiere enviar
                                                                  notificaciones al cliente.
                                                                  En este caso lo hará en el evento "/chat/".
                                                                */
        registry.setApplicationDestinationPrefixes("/app"); /* Se setea el destino de la configuración
                                                           cuando publicamos un mensaje a través del prefijo.
                                                         */
    }
}
