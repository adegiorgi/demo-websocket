package com.example.demowebsocket.controllers;

import com.example.demowebsocket.models.documents.Message;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import java.util.Date;

@Controller
public class ChatController {

    /**
     * Recibe un mensaje de los clientes y lo podemos manejar
     * guardándolo en una DB.
     * @param message mensaje de los clientes.
     * @return        mensaje a reenviar.
     **/
    @MessageMapping("/message") /*
                                    Indicamos el endpoint donde van a enviarse los mensajes.
                                    Es como un GET, POST, DELETE, etc.
                                 */
    @SendTo("/chat/message") /*
                                Con esta anotación definimos el nombre del evento el cual vamos
                                a enviar el mensaje.
                                Todos los nombres de eventos del broker tienen que iniciar con
                                '/chat/' ya que es lo que definimos en la clase WebSocketConfig
                                como prefix.
                              */
    public Message getMessage(Message message) {
        message.setDate(new Date().getTime());
        message.setText("Recibido por el broker: " + message.getText());
        // TODO: Almacenar en bd.
        return message;
    }
}
