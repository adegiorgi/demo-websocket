# DEMO WEBSOCKET - Angular - Spring Boot #

### Información

Se creó este repositorio con el fin de publicar una demo de la implementación de **WebSocket** tanto en front como backend.

Esto se logró siguiendo el curso:

- [Angular & Spring Boot: Creando web app full stack](https://www.udemy.com/course/angular-spring/learn/lecture/13842286#overview)

Se dejaron anotaciones en el código para poder comprender con mayor detalle la forma de implementar **WebSocket** y *cómo* se realiza la comunicación entre el frontend y el backend.

### Artículos de Interés

Además del curso, se consultaron las páginas siguientes:

- [Spring Boot + Angular 8 + WebSocket Example Tutorial](https://www.javaguides.net/2019/06/spring-boot-angular-8-websocket-example-tutorial.html)
- [WebSocket with Spring boot and Angular](https://www.stackextend.com/angular/websocket-with-spring-boot-and-angular/)